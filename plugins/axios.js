export default ({ app: { $axios } }) => {
  $axios.onResponse((response) => {
    console.log('TCL: response', response)

    return response
  })

  $axios.onRequest((config) => {
    console.log('TCL: config', config)
  })
}
