export default {
  strategies: {
    local: {
      endpoints: {
        login: {
          url: '/login',
          method: 'post',
          propertyName: 'access_token'
        },
        logout: {
          url: '/logout',
          method: 'post'
        },
        user: {
          url: '/profile/user',
          method: 'get',
          propertyName: 'user'
        }
      },
      tokenRequired: true
    },
    facebook: {
      client_id: 2551164611646273,
      userinfo_endpoint:
        'https://graph.facebook.com/v2.12/me?fields=email,name',
      scope: ['public_profile', 'email']
    },
    google: {
      client_id:
        '13324402299-gadjpbgt74o2r5qdjnhv0h3v49c96ma7.apps.googleusercontent.com'
    },
    linkedin: {
      _scheme: 'oauth2',
      authorization_endpoint: 'https://www.linkedin.com/oauth/v2/authorization',
      token_endpoint: 'https://www.linkedin.com/oauth/v2/accessToken',
      userinfo_endpoint: 'https://api.linkedin.com/v2/me',
      scope: ['r_basicprofile', 'r_emailaddress'],
      client_id: '78974owmbjcz61',
      response_type: 'code',
      redirect_uri: 'http://localhost:3000'
    }
  }
}
